package solidaria.utiles;


/*    */ 

/*    */ import java.io.FileInputStream;
/*    */ import java.io.IOException;
/*    */ import java.util.HashMap;
/*    */ import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

 public class ReadFile {
/* 22 */   private HashMap params = new HashMap();
private static Logger log = LogManager.getLogger();
/*    */   
/*    */   public HashMap readConfiguration(String archivo, String usuario, String password) {
/* 26 */     Properties properties = new Properties();
/*    */     try
/*    */     {
/* 29 */       FileInputStream in = new FileInputStream(archivo);
/* 30 */       properties.loadFromXML(in);
/*    */       
/* 32 */       this.params.put("User DB", usuario);
/* 33 */       this.params.put("Pass DB", password);
/* 34 */       this.params.put("IP DB", properties.getProperty("IP DB"));
/* 35 */       this.params.put("Port DB", properties.getProperty("Port DB"));
/* 36 */       this.params.put("Database DB", properties.getProperty("Database DB"));
/*    */       
/* 38 */       this.log.info(" user [" + (String)this.params.get("User DB") + "]");
/* 39 */       this.log.info(" pass [" + (String)this.params.get("Pass DB") + "]");
/* 40 */       this.log.info(": IP DB [" + this.params.get("IP DB") + "]");
/* 41 */       this.log.info(": Port DB [" + this.params.get("Port DB") + "]");
/* 42 */       this.log.info(": Database DB [" + this.params.get("Database DB") + "]");
/*    */     }
/*    */     catch (IOException e) {
/* 45 */       this.log.info(": Error reading configuration [" + e.getMessage() + "]");
/*    */     }
/*    */     
/* 48 */     return this.params;
/*    */   }
/*    */ }

