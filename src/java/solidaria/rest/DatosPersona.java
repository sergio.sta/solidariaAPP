/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solidaria.rest;

/**
 *
 * @author sergi
 */
public class DatosPersona {

    private String numero_socio;
    private String nombre;
    private String Deuda_aportes;
    private String Deuda_solidaridad;
    private String Fecha_ingreso;
    private String Atraso;
    private String Error;
    private String Fecha_ult_pago_apo;
    
    private String Fecha_ult_pago_sol ;

    @Override
    public String toString() {
        return "DatosPersona{" + "numero_socio=" + numero_socio + ", nombre=" + nombre + ", Deuda_aportes=" + Deuda_aportes + ", Deuda_solidaridad=" + Deuda_solidaridad + ", Fecha_ingreso=" + Fecha_ingreso + ", Atraso=" + Atraso + ", Error=" + Error + ", Fecha_ult_pago_apo=" + Fecha_ult_pago_apo + ", Fecha_ult_pago_sol=" + Fecha_ult_pago_sol + '}';
    }

    /**
     * @return the numero_socio
     */
    public String getNumero_socio() {
        return numero_socio;
    }

    /**
     * @param numero_socio the numero_socio to set
     */
    public void setNumero_socio(String numero_socio) {
        this.numero_socio = numero_socio;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the Deuda_aportes
     */
    public String getDeuda_aportes() {
        return Deuda_aportes;
    }

    /**
     * @param Deuda_aportes the Deuda_aportes to set
     */
    public void setDeuda_aportes(String Deuda_aportes) {
        this.Deuda_aportes = Deuda_aportes;
    }

    /**
     * @return the Deuda_solidaridad
     */
    public String getDeuda_solidaridad() {
        return Deuda_solidaridad;
    }

    /**
     * @param Deuda_solidaridad the Deuda_solidaridad to set
     */
    public void setDeuda_solidaridad(String Deuda_solidaridad) {
        this.Deuda_solidaridad = Deuda_solidaridad;
    }

    /**
     * @return the Fecha_ingreso
     */
    public String getFecha_ingreso() {
        return Fecha_ingreso;
    }

    /**
     * @param Fecha_ingreso the Fecha_ingreso to set
     */
    public void setFecha_ingreso(String Fecha_ingreso) {
        this.Fecha_ingreso = Fecha_ingreso;
    }

    /**
     * @return the Atraso
     */
    public String getAtraso() {
        return Atraso;
    }

    /**
     * @param Atraso the Atraso to set
     */
    public void setAtraso(String Atraso) {
        this.Atraso = Atraso;
    }

    /**
     * @return the Error
     */
    public String getError() {
        return Error;
    }

    /**
     * @param Error the Error to set
     */
    public void setError(String Error) {
        this.Error = Error;
    }

    /**
     * @return the Fecha_ult_pago_apo
     */
    public String getFecha_ult_pago_apo() {
        return Fecha_ult_pago_apo;
    }

    /**
     * @param Fecha_ult_pago_apo the Fecha_ult_pago_apo to set
     */
    public void setFecha_ult_pago_apo(String Fecha_ult_pago_apo) {
        this.Fecha_ult_pago_apo = Fecha_ult_pago_apo;
    }

    /**
     * @return the Fecha_ult_pago_sol
     */
    public String getFecha_ult_pago_sol() {
        return Fecha_ult_pago_sol;
    }

    /**
     * @param Fecha_ult_pago_sol the Fecha_ult_pago_sol to set
     */
    public void setFecha_ult_pago_sol(String Fecha_ult_pago_sol) {
        this.Fecha_ult_pago_sol = Fecha_ult_pago_sol;
    }
}
