/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solidaria.rest;

import com.google.gson.Gson;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solidaria.clases.RespuestaComun;

/**
 * REST Web Service
 *
 * @author sergi
 */
@Path("consulta")
public class ConsultaResource {
private static Logger log = LogManager.getLogger();


private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String DB_CONNECTION = "jdbc:oracle:thin:@192.168.8.58:1521/cuseg11";
	private static final String DB_USER = "s_panal";
	private static final String DB_PASSWORD = "solidaria123";
         private static HashMap params = new HashMap();
        
        
        
        
    /**
     * Creates a new instance of ConsultaResource
     */
    public ConsultaResource() {
    }
    
    private static Connection getDBConnection() {

		Connection dbConnection = null;

		try {

			Class.forName(DB_DRIVER);

		} catch (ClassNotFoundException e) {

			System.out.println(e.getMessage());

		}

		try {

			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,
					DB_PASSWORD);
			return dbConnection;

		} catch (Exception e) {

			System.out.println(e.getMessage());

		}

		return dbConnection;

	}



    /**
     * Retrieves representation of an instance of solidaria.rest.ConsultaResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("test")
    public String test() throws Exception{
        log.info("Entrada TEST");
                Context ctx = null;
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
			//ctx =  new InitialContext();
			//DataSource ds = (DataSource) ctx.lookup("Solidaria");
                        
    conn = this.getDBConnection();
         // driver@machineName:port:SID           ,  userid,  password
     stmt = conn.createStatement();
    ResultSet rset = 
              stmt.executeQuery("select * from DUAL");
    String res ="";
    while (rset.next())
         res += (rset.getString(1));   // Print col 1
    stmt.close();
    Gson g = new Gson();
     log.info("SALIDA TEST "+ res);
    return g.toJson(res) ;
    
    
    }
    
    
    
     @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("buscar")
    public String buscar(@QueryParam("cedula") String cedula,@QueryParam("fecha") String fecha, @QueryParam("imei") String imei) throws Exception{
     log.info("Entrada:"+cedula+"::"+fecha);
      SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy hh:mm");      
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");    
        
         SimpleDateFormat df3 = new SimpleDateFormat("ddMyyyy");    
        Context ctx = null;
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet ryyyys = null;
		
		//	ctx =  new InitialContext();
		//	DataSource ds = (DataSource) ctx.lookup("Solidaria");
                        
    conn = this.getDBConnection();
    // OracleCallableStatement ocs = 
      //    (OracleCallableStatement)conn.prepareCall(
      //        "{? = call acpks_stmt_gen.fn_stmt_gen(?,?,?,?,?,?)}");
      CallableStatement ocs = 
         conn.prepareCall(
            "{ call panal_solid_situacion_socio(?,?,?,?,?,?,?,?,?,?,?)}");

      
      java.util.Date d =  df.parse(fecha);
      ocs.setInt(1, Integer.parseInt( cedula));
      ocs.setDate(2, new Date(d.getTime()));
     
      ocs.registerOutParameter(3, java.sql.Types.INTEGER);
      ocs.registerOutParameter(4, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(5, java.sql.Types.INTEGER);
      ocs.registerOutParameter(6, java.sql.Types.DATE);
      ocs.registerOutParameter(7, java.sql.Types.INTEGER);
      ocs.registerOutParameter(8, java.sql.Types.DATE); 
      ocs.registerOutParameter(9, java.sql.Types.DATE);
      ocs.registerOutParameter(10, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(11, java.sql.Types.VARCHAR);
      



      ocs.executeUpdate(); 

     java.sql.ResultSet rs2 = (java.sql.ResultSet) ocs.getResultSet();
      DatosPersona pr = new DatosPersona();
     
        pr.setNumero_socio( ocs.getString(3));
        pr.setNombre(ocs.getString(4));
        pr.setDeuda_aportes(ocs.getInt(5)+"");
        if(ocs.getDate(6)!=null)
        pr.setFecha_ult_pago_apo(df2.format(ocs.getDate(6)));
        pr.setDeuda_solidaridad(ocs.getInt(7)+"");
        if(ocs.getDate(8)!=null)       
        pr.setFecha_ult_pago_sol(df2.format(ocs.getDate(8)));        
        if(ocs.getDate(9)!=null)   
        pr.setFecha_ingreso(df.format(ocs.getDate(9)));   
        pr.setAtraso(ocs.getString(10));


        pr.setError(ocs.getString(11));

      //dd/mm/AAAA hh:mm
     
      //pr.setFecha_ult_pago_apo(df2.format(new java.util.Date()));
      //pr.setFecha_ult_pago_sol(df2.format(new java.util.Date()));
      
      ocs.close();
      conn.close();
              
              
              
    Gson g = new Gson();
     log.info("variable:"+pr);
     String salida=""+g.toJson(pr, DatosPersona.class);
    log.info("Salida:"+salida);
    params.put("ENTRADA", cedula+"::"+fecha);
    params.put("SALIDA",salida);
    params.put("IMEI",imei);
    params.put("USUARIOW", "");
    Validacion v = new  Validacion();
    Connection conexion = v.db.conectarBD(params);
    RespuestaComun r = new RespuestaComun();
    
    v.Registrar(r,"" , conexion, params);
    
    return g.toJson(pr, DatosPersona.class) ;
    }
    
    
    /*
    
    CREATE OR REPLACE procedure CUSEG.panal_solid_situacion_socio( pDocumento           IN NUMBER
                                                       , pFecha               IN DATE
                                                       , pNumero_socio       OUT NUMBER
                                                       , pNombre_completo    OUT VARCHAR2 
                                                       , pDeuda_aportes      OUT NUMBER
                                                       , pUlt_fecha_pago_ap  OUT DATE
                                                       , pDeuda_solidaridad  OUT NUMBER
                                                       , pUlt_fecha_pago_so  OUT DATE
                                                       , pFecha_ingreso      OUT DATE
                                                       , patraso             OUT VARCHAR2
                                                       , pError              OUT VARCHAR2) 
    
    
    
    
    CREATE OR REPLACE procedure CUSEG.panal_solid_situacion_soc_act( pDocumento           IN NUMBER
                                                       , pFecha               IN DATE
                                                     1  , pNumero_socio       OUT NUMBER
                                                     2 , pNombre_completo    OUT VARCHAR2
                                                     3  , pDeuda_aportes      OUT NUMBER
                                                     4  , pUlt_fecha_pago_ap  OUT DATE
                                                     5  , pDeuda_solidaridad  OUT NUMBER
                                                     6  , pUlt_fecha_pago_so  OUT DATE
                                                     7  , pFecha_ingreso      OUT DATE
                                                     8 , patraso             OUT VARCHAR2
                                                     9  , pError              OUT VARCHAR2
                                                     10  , pSITUACION_SOCIO OUT VARCHAR2
                                                     11  ,pTELEFONO1      OUT VARCHAR2
                                                     12  ,pTELEFONO2      OUT VARCHAR2
                                                     13  ,pTELEFONO3      OUT VARCHAR2)
    */
     
     @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("buscar_act")
    public String buscar_act(@QueryParam("cedula") String cedula,@QueryParam("fecha") String fecha, @QueryParam("imei") String imei) throws Exception{
     log.info("Entrada:"+cedula+"::"+fecha);
      SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy hh:mm");      
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");    
        
         SimpleDateFormat df3 = new SimpleDateFormat("ddMyyyy");    
        Context ctx = null;
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet ryyyys = null;
		
		//	ctx =  new InitialContext();
		//	DataSource ds = (DataSource) ctx.lookup("Solidaria");
                        
    conn = this.getDBConnection();
    try{
    // OracleCallableStatement ocs = 
      //    (OracleCallableStatement)conn.prepareCall(
      //        "{? = call acpks_stmt_gen.fn_stmt_gen(?,?,?,?,?,?)}");
      CallableStatement ocs = 
         conn.prepareCall(
            "{ call panal_solid_situacion_soc_act(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

      
      java.util.Date d =  df.parse(fecha);
      ocs.setInt(1, Integer.parseInt( cedula));
      ocs.setDate(2, new Date(d.getTime()));
     
      ocs.registerOutParameter(3, java.sql.Types.INTEGER);
      ocs.registerOutParameter(4, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(5, java.sql.Types.INTEGER);
      ocs.registerOutParameter(6, java.sql.Types.DATE);
      ocs.registerOutParameter(7, java.sql.Types.INTEGER);
      ocs.registerOutParameter(8, java.sql.Types.DATE); 
      ocs.registerOutParameter(9, java.sql.Types.DATE);
      ocs.registerOutParameter(10, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(11, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(12, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(13, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(14, java.sql.Types.VARCHAR);
      ocs.registerOutParameter(15, java.sql.Types.VARCHAR);


      ocs.executeUpdate(); 

     java.sql.ResultSet rs2 = (java.sql.ResultSet) ocs.getResultSet();
      DatosPersonaAct pr = new DatosPersonaAct();
     
        pr.setNumero_socio( ocs.getString(3));
        pr.setNombre(ocs.getString(4));
        pr.setDeuda_aportes(ocs.getInt(5)+"");
        if(ocs.getDate(6)!=null)
        pr.setFecha_ult_pago_apo(df2.format(ocs.getDate(6)));
        pr.setDeuda_solidaridad(ocs.getInt(7)+"");
        if(ocs.getDate(8)!=null)       
        pr.setFecha_ult_pago_sol(df2.format(ocs.getDate(8)));        
        if(ocs.getDate(9)!=null)   
        pr.setFecha_ingreso(df.format(ocs.getDate(9)));   
        pr.setAtraso(ocs.getString(10));


        pr.setError(ocs.getString(11));
        
        pr.setpSITUACION_SOCIO(ocs.getString(12));
        pr.setpTELEFONO1(ocs.getString(13));
         pr.setpTELEFONO2(ocs.getString(14));
         pr.setpTELEFONO3(ocs.getString(15));

      //dd/mm/AAAA hh:mm
     
      //pr.setFecha_ult_pago_apo(df2.format(new java.util.Date()));
      //pr.setFecha_ult_pago_sol(df2.format(new java.util.Date()));
      
      ocs.close();
      conn.close();
               
              
    Gson g = new Gson();
     log.info("variable:"+pr);
    log.info("Salida:"+g.toJson(pr, DatosPersonaAct.class));
    
    params.put("ENTRADA", cedula+"::"+fecha);
    params.put("SALIDA",pr.toString() );
    params.put("IMEI",imei);
    params.put("USUARIOW", "");
    Validacion v = new  Validacion();
    Connection conexion = v.db.conectarBD(params);
    RespuestaComun r = new RespuestaComun();
    
    v.Registrar(r,"" , conexion, params);
    
     return g.toJson(pr, DatosPersonaAct.class) ;
     }catch(Exception e){
        log.error("STACk", e);
        conn.close();
     }
   
      return null;    
    
    }

    /**
     * PUT method for updating or creating an instance of ConsultaResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void putText(String content) {
    }
}
