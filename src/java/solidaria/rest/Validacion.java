/*     */ package solidaria.rest;
/*     */ 
/*     */ import com.google.gson.Gson;


/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.sql.Connection;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.Statement;
/*     */ import java.util.HashMap;
/*     */ import java.util.StringTokenizer;
/*     */ import javax.ejb.Stateless;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.QueryParam;
        
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solidaria.bd.BD;
import solidaria.clases.RespuestaComun;
import solidaria.utiles.ReadFile;
import solidaria.utiles.StringUtils;


/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Stateless
/*     */ @Path("/validacion")
/*     */ public class Validacion
/*     */ {
/*     */   private static HashMap params = new HashMap();
private static Logger log = LogManager.getLogger();
/*     */   
/*     */   private static String idLog;
/*  40 */   public final BD db = new BD();
/*  41 */   private final StringUtils StringUtils = new StringUtils();
            private ConsultaResource oracle = new ConsultaResource();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private Statement stmt;
/*     */   
/*     */ 
/*     */ 
 @Path("/web")
@GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta_web(@QueryParam("usuario") String usuario, @QueryParam("password") String password, @QueryParam("usuariow") String usuariow,@QueryParam("passw") String passw,@QueryParam("cedula") String cedula,@QueryParam("fecha") String fecha)
{ 
     String resultado = "{\"Error\":\"Sin conexion\"}";
    try{
        Gson g = new Gson();

   resultado =  this.consulta(usuario, password, "", "", "", usuariow, passw);
   
   RespuestaComun r =  g.fromJson(resultado, RespuestaComun.class);
   if(!r.getEstado().equals("OK")){
       return "{\"Error\":\"credenciales incorrectas\"}";
   }
    resultado =  oracle.buscar_act(cedula, fecha,"");
    params.put("ENTRADA", cedula+"::"+fecha);
    params.put("SALIDA",resultado);
    params.put("IMEI","");
    params.put("USUARIOW", usuariow);
    
    Connection conexion = this.db.conectarBD(params);
    this.Registrar(r, idLog, conexion, params);
   }catch(Exception e){
       log.error("STACK",e);
   }
    return resultado;
}




/*     */ 
/*     */ 
@Path("/app")
/*     */   @GET
/*     */   @Consumes({"application/json"})
/*     */   public String consulta(@QueryParam("usuario") String usuario, @QueryParam("password") String password,  @QueryParam("imei") String imei, @QueryParam("latitud") String latitud, @QueryParam("longitud") String longitud,@QueryParam("usuariow") String usuariow,@QueryParam("passw") String passw)
/*     */     throws Exception
/*     */   {
/*  58 */     Connection conexion = null;
/*  59 */     RespuestaComun respuesta = new RespuestaComun();
/*     */     
/*     */     try
/*     */     {
/*     */       try
/*     */       {
/*  65 */         idLog = "1";
/*     */         
/*  67 */         ReadFile file = new ReadFile();
/*  68 */       //  params = file.readConfiguration("HAPOITE.xml", usuario, password);
/*     */       } catch (Exception ex) {
/*  70 */         log.error("[" + idLog + "] ACTIVACION - Error reading configuration [" + ex.getMessage() + "]");
/*  71 */         respuesta.setEstado("ERROR");
/*  72 */         respuesta.setMensaje("INTERNAL #1");
/*  73 */         throw new Exception("Error reading configuration [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*  76 */       log.info("[" + idLog + "] ## Activacion - Parametros ##");
/*  77 */       log.info("[" + idLog + "] " + "usuario [" + usuario + "]");
/*  78 */       log.info("[" + idLog + "] " + "password [" + password + "]");

/*  81 */       log.info("[" + idLog + "] " + "imei [" + imei + "]");
/*  82 */       log.info("[" + idLog + "] " + "latitud [" + latitud + "]");
/*  83 */       log.info("[" + idLog + "] " + "longitud [" + longitud + "]");
/*     */       
/*  85 */   //    log.error("[" + idLog + "] Activacion: cedula->" + cedula + ", telefono->" + telefono);
/*     */       
/*     */       try
/*     */       {
/*  89 */         conexion = this.db.conectarBD(params);
/*     */       } catch (Exception ex) {
/*  91 */         log.error("[" + idLog + "] ACTIVACION - Error when connecting to database  [" + ex.getMessage() + "]");
                    log.error("stack",ex);
/*  92 */         respuesta.setEstado("ERROR");
/*  93 */         respuesta.setMensaje("CREDENCIALES INCORRECTAS");
/*  94 */         throw new Exception(ex.getMessage());
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*  99 */     //  params.put("Cedula", cedula);
/* 100 */     //  params.put("NroTelefono", telefono);
/* 101 */       params.put("IMEI", imei);
/* 102 */       params.put("lat", latitud);
/* 103 */       params.put("long", longitud);
                if(usuariow == null) usuariow = "";
                if(passw == null) passw = "";
                params.put("USUARIOW",usuariow);
                 params.put("PASSW",passw);
/*     */       
/*     */       try
/*     */       {
/* 107 */         respuesta = ProcesarActivacion(respuesta, idLog, conexion, params);
                   
/*     */       }
/*     */       catch (Exception ex) {
/* 110 */         respuesta.setEstado("ERROR");
/* 111 */         respuesta.setMensaje("INTERNAL #2");
/* 112 */         log.info("[" + idLog + "] ACTIVACION - Error #2[" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */       try
/*     */       {
/* 124 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 126 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */       
/*     */ 
/* 130 */       log.info("[" + idLog + "] estado [" + respuesta.getEstado() + "]");
/*     */     }
/*     */     catch (Exception ex)
/*     */     {
/* 116 */       if (respuesta.getEstado() == null) {
/* 117 */         respuesta.setEstado("ERROR");
/* 118 */         respuesta.setMensaje("INTERNAL #3");
/*     */       }
/* 120 */       log.error("[" + idLog + "] ACTIVACION - Error Process #3 [" + ex.getMessage() + "]");
/*     */     }
/*     */     finally {
/*     */       try {
/* 124 */         this.db.desconectarBD(conexion);
/*     */       } catch (Exception ex) {
/* 126 */         log.error("[" + idLog + "] Could not disconnect from the database [" + ex.getMessage() + "]");
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 131 */     log.info("[" + idLog + "] mensaje [" + respuesta.getMensaje() + "]");
/*     */     
/* 133 */     ByteArrayOutputStream baos = new ByteArrayOutputStream();
/*     */     
/*     */ 
/*     */ 
/* 137 */     Gson gson = new Gson();
/* 138 */     String respuestaFinal = gson.toJson(respuesta);
/*     */     
/* 140 */     log.info("[" + idLog + "] respuesta [" + respuesta + "]");
/*     */     
/* 142 */     return respuestaFinal;
/*     */   }
/*     */   
/*     */   private RespuestaComun ProcesarActivacion(RespuestaComun respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 147 */     ResultSet resultSet = null;
/* 148 */     String query = null;
/*     */     
/*     */     try
/*     */     {
/* 152 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Activacion]");
/*     */       /*

CREATE FUNCTION _WebS_Control_IMEI_USER ( mIMEI    Char(30)    , 
                                          mUsuario  Char(16)   , 
                                          mPWD      VarChar(50) )
RETURNS Varchar(100)

*/

/* 154 */       this.stmt = conexion.createStatement();
/* 155 */       query = "SELECT DBA._WebS_Control_IMEI_USER  ('" + params.get("IMEI")  + "','" + params.get("USUARIOW")  + "','" + params.get("PASSW")  + "') resultado";
/*     */      // query = "SELECT dba._WebS_Activacion ('" + params.get("Cedula") + "', '" + params.get("NroTelefono") + "', '" + params.get("IMEI") + "', '" + params.get("lat") + "', '" + params.get("long") + "') resultado";
/*     */       
/*     */ 
/*     */ 
/* 159 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 160 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 162 */       if (resultSet.next())
/*     */       {
/* 164 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/*     */         
/* 166 */         StringTokenizer tokens = new StringTokenizer(resultSet.getString("resultado"), "#");
/*     */         
/* 168 */         respuesta.setEstado(tokens.nextToken());
/* 169 */         respuesta.setMensaje(tokens.nextToken());
/* 170 */         conexion.commit();
/*     */       }
/*     */       else {
/* 173 */         respuesta.setEstado("ERROR");
/* 174 */         respuesta.setMensaje("INTERNAL #4");
/*     */       }
/*     */     }
/*     */     catch (Exception ex) {
/* 178 */       log.error("[" + idLog_ + "] ACTIVACION - Error #5[" + ex.getMessage() + "]");
/* 179 */       respuesta.setEstado("ERROR");
/* 180 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }
/* 182 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 183 */     return respuesta;
/*     */   }



/*     */   public RespuestaComun Registrar(RespuestaComun respuesta, String idLog_, Connection conexion, HashMap params)
/*     */   {
/* 147 */     ResultSet resultSet = null;
/* 148 */     String query = null;
/*     */     
/*     */     try
/*     */     {
/* 152 */       log.info("[" + idLog_ + "] Preparando la funcion [dba._WebS_Activacion]");
/*     */       /*

CREATE FUNCTION _WebS_Control_IMEI_USER ( mIMEI    Char(30)    , 
                                          mUsuario  Char(16)   , 
                                          mPWD      VarChar(50) )
RETURNS Varchar(100)

*/

/* 154 */       this.stmt = conexion.createStatement();
/* 155 */       query = "SELECT DBA._WebS_Consulta_Registro  ('" + params.get("IMEI")  + "','" + params.get("USUARIOW")  + "','" + params.get("ENTRADA")  + "','" + params.get("SALIDA")  + "') resultado";
/*     */      // query = "SELECT dba._WebS_Activacion ('" + params.get("Cedula") + "', '" + params.get("NroTelefono") + "', '" + params.get("IMEI") + "', '" + params.get("lat") + "', '" + params.get("long") + "') resultado";
/*     */       
/*     */ 
/*     */ 
/* 159 */       log.info("[" + idLog_ + "] Query: [" + query + "]");
/* 160 */       resultSet = this.stmt.executeQuery(query);
/*     */       
/* 162 */       if (resultSet.next())
/*     */       {
/* 164 */         log.info("[" + idLog_ + "] resultado: [" + resultSet.getString("resultado") + "]");
/*     */         
/* 166 */         StringTokenizer tokens = new StringTokenizer(resultSet.getString("resultado"), "#");
/*     */         
/* 168 */         respuesta.setEstado(tokens.nextToken());
/* 169 */         respuesta.setMensaje(tokens.nextToken());
/* 170 */         conexion.commit();
/*     */       }
/*     */       else {
/* 173 */         respuesta.setEstado("ERROR");
/* 174 */         respuesta.setMensaje("INTERNAL #4");
/*     */       }
/*     */     }
/*     */     catch (Exception ex) {
/* 178 */       log.error("[" + idLog_ + "] ACTIVACION - Error #5[" + ex.getMessage() + "]");
/* 179 */       respuesta.setEstado("ERROR");
/* 180 */       respuesta.setMensaje("INTERNAL #5");
/*     */     }
/* 182 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
/* 183 */     return respuesta;
/*     */   }
/*     */ }

/*     */ 


/* Location:              C:\Users\sergi\OneDrive\Documents\Hapoite6122732133184831827.war!\WEB-INF\classes\py\com\hapoite\main\Activacion.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */


