/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solidaria.bd;




import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BD
{
  private static Logger log = LogManager.getLogger();
  
  public Connection conectarBD(HashMap params)
    throws Exception
  {
    Class.forName("com.sybase.jdbc3.jdbc.SybDriver");
    Properties props = new Properties();
    props.put("user", "usrwebservice");
    props.put("password", "S0l1dar1aWS");
    String url = "jdbc:sybase:Tds:" + "192.168.150.250" + ":" + "2638";
    this.log.info(" url [" + url + "]");
    Connection conn = DriverManager.getConnection(url, props);
    conn.setAutoCommit(false);
    return conn;
  }
  
  public void desconectarBD(Connection pConn)
    throws SQLException
  {
    pConn.close();
  }
}
